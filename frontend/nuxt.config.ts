// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    devtools: {enabled: true},
    build: {transpile: ["@fawmi/vue-google-maps"]},
    modules: ['@pinia/nuxt', '@ant-design-vue/nuxt'
    ],
    runtimeConfig: {
        public: {
            API_URL: process.env.API_URL,
            MAP_API_KEY: process.env.MAP_API_KEY
        }
    },
    plugins: ['@/plugins/gmaps']
    , vite: {
        optimizeDeps: {
            include: [
                "@fawmi/vue-google-maps",
                "fast-deep-equal",
            ],
        }
    }
})
