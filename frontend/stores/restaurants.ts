import {defineStore} from 'pinia'

const list: any[] = []
export const useRestaurantStore = defineStore('restaurant', {
    state: () => ({
        list: list,
        searchLocation: 'Bang Sue',
        nextPageToken: '',
        center: null,
        loading: false
    }),
    actions: {
        async fetchList(location: string, nextPageToken: string | null = null) {
            this.loading = true
            const runtimeConfig = useRuntimeConfig();
            this.searchLocation = location
            const {data}: any = await useFetch(
                runtimeConfig.public.API_URL + '/restaurants/' + location
                , {query: {nextPageToken: nextPageToken}})
            const {value} = data
            this.loading = false
            if (value.status == 'OK') {
                const {results} = value;
                if (!nextPageToken)
                    this.list = results
                else
                    this.list.push(...results)


                this.nextPageToken = value.next_page_token
                this.center = value.center
            }

        }
    },
    getters: {
        getList: (state) => state.list,
        getNextPageToken: (state) => state.nextPageToken,
        getSearchLocation: (state) => state.searchLocation,
        getLoading: (state) => state.loading,
        getCenter: (state) => state.center,
    },


})
