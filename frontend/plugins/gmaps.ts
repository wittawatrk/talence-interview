import {defineNuxtPlugin} from "#app";

import VueGoogleMaps from "@fawmi/vue-google-maps"

export default defineNuxtPlugin((nuxtApp) => {
    const runtimeConfig = useRuntimeConfig();
    nuxtApp.vueApp.use(VueGoogleMaps, {
        load: {
            key: runtimeConfig.public.MAP_API_KEY,
        },
    })
})
