<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;

class PlaceApiService
{
    private $key;
    private $client;

    /**
     * RestaurantController constructor.
     */
    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client(
            ['base_uri' => 'https://maps.googleapis.com', 'timeout' => 3]);
        $this->key = env('MAP_API_KEY', null);
    }

    function getRestaurants($location, $pageToken)
    {
        $key = $pageToken ? "{$location}{$pageToken}" : $location;
        if (!$restaurants = $this->checkCache($key)) {
            $restaurants = $this->getDataFromGoogleApi($location, $pageToken);
        }
        return response()->json($restaurants, 200);
    }

    function checkCache($key)
    {
        if (Cache::has($key)) {
            return Cache::get($key);
        } else {
            return false;
        }
    }

    function getDataFromGoogleApi($location, $pageToken = null)
    {
        $url = $pageToken ? "/maps/api/place/textsearch/json?pagetoken={$pageToken}&key={$this->key}" : "/maps/api/place/textsearch/json?query=restaurants+in+{$location}&key={$this->key}";
        $result = $this->client->get($url);
        $restaurants = json_decode($result->getBody(), true);
        if (!$restaurants['status'] == 'OK') {
            return false;
        }
        if ($geoLocation = $this->getGeoLocation($location)) {
            $restaurants['center'] = $geoLocation;
        }
        Cache::put($location, $restaurants, Carbon::now()->addDay());
        return $restaurants;
    }

    function getGeoLocation($location)
    {
        if ($location) {
            $result = $this->client->get("/maps/api/geocode/json?address=$location&key=" . $this->key);
            $result = json_decode($result->getBody(), true);
            if ($result['status'] == 'OK') {
                return $result['results'];
            }
            return false;
        }
    }
}
