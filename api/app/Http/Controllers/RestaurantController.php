<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Services\PlaceApiService;
use Illuminate\Support\Facades\Cache;

class RestaurantController extends Controller
{
    private $service;

    /**
     * RestaurantController constructor.
     */
    public function __construct(PlaceApiService $placeApiService)
    {
        $this->service = $placeApiService;
    }

    /**
     * Get Restaurants
     *
     * @return string
     *
     * @SWG\Get(
     *        path = "/restaurants/{location}" ,
     *        description = "Get Restaurants by Location." ,
     *        operationId = "restaurantSearch" ,
     *        produces = { "application/json" } ,
     *        tags = { "restaurant" } ,
     *		@SWG\Parameter(
     *            name = "location" ,
     *            in = "path" ,
     *          description = "place to search"
     *            required = true ,
     *            type = "string"
     *        ) ,
     *     @SWG\Parameter(
     *            name = "nextPageToken" ,
     *            in = "query" ,
     *          description = "next  page token to query data"
     *            required = true ,
     *            type = "string"
     *        ) ,
     *		@SWG\Response(
     *            response = 200 ,
     *            description = "success"
     *        ) ,
     * )
     */
    function getRestaurants(Request $request, $location)
    {
        $pageToken = $request->input('nextPageToken') ?? null;

        return $this->service->getRestaurants($location, $pageToken);
    }
}
